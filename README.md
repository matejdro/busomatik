# Busomatik

Open source Android base application for viewing public transit information in Slovenia. Not intended to be a single app, but rather a white-label codebase that can be separately built to best suit each supported city's available transit data.

## History / About

This application is a fork of the excellent [Travana](https://github.com/siggsy/travana-android) app, which was inteded for use only in Ljubljana. That project is now in maintenance mode, as the authors are working on a full rewrite.   
When we stared working on a unified API for all the other transit providers in Slovenia around 2021, we decided to use their code as the base of our frontend.   

This fork is focused on de-coupling Travana from the specifics of the LPP API, making it near-trivial to plug in data for other cities.   
Our current focus is on publishing a modified version for Maribor as a proof of concept in order to get more transit providers on board.

## Where do I get it?

We have not yet published any Busomatik-based apps. If you would like to try a development build, you can browse the artifacts from our CI jobs on GitLab.
