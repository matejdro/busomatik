package com.VegaSolutions.lpptransit.utility;

import com.VegaSolutions.lpptransit.lppapi.responseobjects.ArrivalOnRoute;
import com.VegaSolutions.lpptransit.lppapi.responseobjects.ArrivalWrapper;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class TimeUtil {
    static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public static String formatArrival(int etaMin, String arrivalTime, boolean useAbsoluteTime) {
        if (useAbsoluteTime) {
            if (arrivalTime != null) {
                return arrivalTime;
            } else {
                return formatter.format(DateTime.now().plusMinutes(etaMin).toDate());
            }
        } else {
            return String.format("%s min", etaMin);
        }
    }

    public static String formatArrival(ArrivalOnRoute.Arrival arrival, boolean useAbsoluteTime) {
        return formatArrival(arrival.getEtaMin(), arrival.getArrivalTime(), useAbsoluteTime);
    }

    public static String formatArrival(ArrivalWrapper.Arrival arrival, boolean useAbsoluteTime) {
        return formatArrival(arrival.getEtaMin(), arrival.getArrivalTime(), useAbsoluteTime);
    }
}
