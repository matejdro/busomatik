package com.VegaSolutions.lpptransit.utility;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

public class Colors {

    public static List<Integer> colors;


    static {
        colors = new ArrayList<>();
        colors.add(Color.parseColor("#000000"));
        colors.add(Color.parseColor("#0081a7"));
        colors.add(Color.parseColor("#00afb9"));
        colors.add(Color.parseColor("#f6bd60"));
        colors.add(Color.parseColor("#f07167"));
        colors.add(Color.parseColor("#4a4e69"));
        colors.add(Color.parseColor("#c9ada7"));
        colors.add(Color.parseColor("#ddb892"));
        colors.add(Color.parseColor("#7f5539"));
        colors.add(Color.parseColor("#656d4a"));
        colors.add(Color.parseColor("#000000"));
        colors.add(Color.parseColor("#081c15"));
        colors.add(Color.parseColor("#e71d36"));
        colors.add(Color.parseColor("#000000"));
        colors.add(Color.parseColor("#fe7f2d"));
        colors.add(Color.parseColor("#fcca46"));
        colors.add(Color.parseColor("#720026"));
        colors.add(Color.parseColor("#736f72"));
        colors.add(Color.parseColor("#b32d00"));
        colors.add(Color.parseColor("#e27396"));
        colors.add(Color.parseColor("#a3a375"));
        colors.add(Color.parseColor("#5c85d6"));
        colors.add(Color.parseColor("#68a691"));
        colors.add(Color.parseColor("#a6b1e1"));
        colors.add(Color.parseColor("#02040f"));
    }

    /**
     * Returns a color corresponding to the number in the string
     * @param s string containing a number between 0 and 100.
     * @return int representing the color.
     */
    public static int getColorFromString(String s) {
        String color = s.replaceAll("[^0-9]", "");
        try {
            return colors.get(Integer.parseInt(color));

        } catch (IndexOutOfBoundsException e) {
            return Color.GRAY;
        }
    }

}
