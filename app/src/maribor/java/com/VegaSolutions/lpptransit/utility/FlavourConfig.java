package com.VegaSolutions.lpptransit.utility;

import com.VegaSolutions.lpptransit.BusomatikApp;
import com.VegaSolutions.lpptransit.lppapi.responseobjects.DetourInfo;
import com.VegaSolutions.lpptransit.lppapi.responseobjects.Station;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MARIBOR flavour config
 */
public class FlavourConfig implements FlavourConfigBase {
    static int UNKNOWN = 0;
    static int TO_CENTER = 2;
    static int FROM_CENTER = 1;

    public LatLng getMapCenter() {
        return new LatLng(46.5547, 15.6459);
    }

    public static String DETOUR_URL = "https://www.marprom.si/feed/";

    public static Station getOppositeStation(BusomatikApp app, Station stationCode) {
        if (!app.areStationsLoaded()) {
            return null;
        }

        for (Station station : app.getStations()) {
            if (station.getRefId().equals(station.getOpposite())) {
                return station;
            }
        }
        return null;
    }

    public boolean isStationCenter(Station s) {
        return s.getDir() == TO_CENTER;
    }

    public static List<DetourInfo> parseDetours(String html) {
        List<DetourInfo> list = new ArrayList<>();

        Pattern detourPattern = Pattern.compile("<item>.+?<title>(.+?)</title>.+?<link>(.+?)</link>.+?<pubDate>(.+?)</pubDate>", Pattern.DOTALL);
        Matcher detourMatcher = detourPattern.matcher(html);
        while (detourMatcher.find()) {
            String title = detourMatcher.group(1);
            if (!title.toLowerCase().contains("obvoz"))
                continue;
            String href = detourMatcher.group(2);
            String date = detourMatcher.group(3);
            list.add(new DetourInfo(title, date, href));
        }

        return list;
    }


}
