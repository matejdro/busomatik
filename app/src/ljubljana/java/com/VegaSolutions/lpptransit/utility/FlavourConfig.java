package com.VegaSolutions.lpptransit.utility;


import com.VegaSolutions.lpptransit.lppapi.responseobjects.DetourInfo;
import com.VegaSolutions.lpptransit.lppapi.responseobjects.Station;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * LJUBLJANA flavour config
 */
public class FlavourConfig implements FlavourConfigBase {
    public static final String DETOUR_URL = "https://www.lpp.si/javni-prevoz/obvozi/";


    LatLng mapCenter = new LatLng(46.5547, 15.6459);

    public boolean isStationCenter(Station s) {
        return Integer.parseInt(s.getRefId()) % 2 != 0;
    }

    public static List<DetourInfo> parseDetours(String html) {
        List<DetourInfo> list = new ArrayList<>();

        Pattern detourPattern = Pattern.compile("<div class=\"content__box--title\"><a href=\"(.*)\">(.*)</a></div>[\\s\\S]*?<div class=\"content__box--date\">(.*)</div>");
        Matcher detourMatcher = detourPattern.matcher(html);
        while (detourMatcher.find()) {
            String href = "https://www.lpp.si" + detourMatcher.group(1);
            String title = detourMatcher.group(2);
            String date = detourMatcher.group(3);
            list.add(new DetourInfo(title, date, href));
        }

        return list;
    }
    
    public static Station getOppositeStation(BusomatikApp app, int station) {
        if (!app.areStationsLoaded()) {
            return null;
        }

        for (Station station : app.getStations()) {
            if (station.getRefId().equals(station.getRefId() + "")) {
                return station;
            }
        }
        return null;
    }


}
